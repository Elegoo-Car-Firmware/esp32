/*
 * SPDX-FileCopyrightText: 2021-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Unlicense OR CC0-1.0
 */

#include "driver/gpio.h"
#include "esp_camera.h"
#include "esp_check.h"
#include "esp_event.h"
#include "esp_heap_caps.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "nvs_flash.h"
#include <freertos/task.h>

#include "sdkconfig.h"
#include <pthread.h>

#include "httpd.h"
#include "wifi.h"
#include "wifi_creds.h"

#include "control_stream.h"

#include "sdkconfig.h"

// Just a macro to simplify GPIO bitmask computation.
#define GPIO_PIN_BIT(x) 1ULL << x

// Led Pin.
#define LED_PIN GPIO_NUM_13

// Camera Pins.
#define CAM_PIN_PWDN -1
#define CAM_PIN_RESET GPIO_NUM_15
#define CAM_PIN_XCLK GPIO_NUM_27
#define CAM_PIN_SCCB_SDA GPIO_NUM_22
#define CAM_PIN_SCCB_SCL GPIO_NUM_23
// Camera uses "default 8-bit connection" mode, so
// Cam Y2 pin connects to controller pin_0, Y3 => pin_1 etc.
#define CAM_PIN_Y9 GPIO_NUM_19
#define CAM_PIN_Y8 GPIO_NUM_36
#define CAM_PIN_Y7 GPIO_NUM_18
#define CAM_PIN_Y6 GPIO_NUM_39
#define CAM_PIN_Y5 GPIO_NUM_5
#define CAM_PIN_Y4 GPIO_NUM_34
#define CAM_PIN_Y3 GPIO_NUM_35
#define CAM_PIN_Y2 GPIO_NUM_32
#define CAM_PIN_VSYNC GPIO_NUM_25
#define CAM_PIN_HREF GPIO_NUM_26
#define CAM_PIN_PCLK GPIO_NUM_21
// End camera pins.

void *blink_led(void *arg) {
  const int delay = 1000 / portTICK_PERIOD_MS;
  int ret;

  while (true) {

    esp_err_t e;

    ESP_LOGD("GPIO_LED", "Set 0");
    e = gpio_set_level(LED_PIN, 0);

    ESP_GOTO_ON_ERROR(e, exit_err, "GPIO_LED", "Set 0 failed");
    vTaskDelay(delay);

    ESP_LOGD("GPIO", "Set 1");
    e = gpio_set_level(LED_PIN, 1);
    ESP_GOTO_ON_ERROR(e, exit_err, "GPIO_LED", "Set 1 failed");
    vTaskDelay(delay);

    ESP_LOGI("MEM", "Free Memory: %zu",
             heap_caps_get_free_size(MALLOC_CAP_DEFAULT));
    ESP_LOGI("MEM", "Free Min Memory: %zu",
             heap_caps_get_minimum_free_size(MALLOC_CAP_DEFAULT));
    printf("\n");
  }

exit_err:
  if (ret != ESP_OK) {
    ESP_LOGE("GPIO_LED", "Fatal error, restarting CPU");
    esp_restart();
  }

  return 0;
}

esp_err_t camera_configure() {
  camera_config_t camconf = {
      .pin_pwdn = CAM_PIN_PWDN,
      .pin_reset = CAM_PIN_RESET,
      .pin_xclk = CAM_PIN_XCLK,
      .pin_sccb_sda = CAM_PIN_SCCB_SDA,
      .pin_sccb_scl = CAM_PIN_SCCB_SCL,

      .pin_d7 = CAM_PIN_Y9,
      .pin_d6 = CAM_PIN_Y8,
      .pin_d5 = CAM_PIN_Y7,
      .pin_d4 = CAM_PIN_Y6,
      .pin_d3 = CAM_PIN_Y5,
      .pin_d2 = CAM_PIN_Y4,
      .pin_d1 = CAM_PIN_Y3,
      .pin_d0 = CAM_PIN_Y2,

      .pin_vsync = CAM_PIN_VSYNC,
      .pin_href = CAM_PIN_HREF,
      .pin_pclk = CAM_PIN_PCLK,

      .xclk_freq_hz = 16000000,
      .ledc_timer = LEDC_TIMER_0,
      .ledc_channel = LEDC_CHANNEL_0,
      // If not JPEG, then FRAMESIZE_QVGA is the largest working.
      .pixel_format = PIXFORMAT_JPEG,
      .frame_size = FRAMESIZE_VGA,
      .jpeg_quality = 20,
      .fb_count = 3,
      .grab_mode = CAMERA_GRAB_WHEN_EMPTY};

  return esp_camera_init(&camconf);
}

esp_err_t control_uart_configure() {
  uart_config_t ucfg = {
      .baud_rate = 115200,
      .data_bits = UART_DATA_8_BITS,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .parity = UART_PARITY_DISABLE,
      // Not sure if it is related but APB clock source
      // causes the port to output at some strange unpredictable baud rate.
      // https://github.com/espressif/esp-idf/issues/7009
      .source_clk = UART_SCLK_REF_TICK,
  };

  struct control_uart_config config = {
      .uart_num = UART_NUM_1,
      .rx_io_num = GPIO_NUM_33,
      .tx_io_num = GPIO_NUM_4,
      .uart_config = ucfg,
  };

  return control_configure_uart(&config);
}

esp_err_t car_report_cb(struct control_status_report *status,
                        void *user_context) {
  switch (status->msg_type) {
  case control_car_message_type_report:
    return httpd_stream_from_car_cb(status, user_context);
  case control_car_message_type_reset:
    esp_restart();
    return ESP_OK;

  default:
    ESP_LOGE("MAIN", "car_report_cb: unknown car report message type: %d",
             status->msg_type);
    return ESP_FAIL;
  }
}

// Main app.
void app_main(void) {
  int ret = 0;
  esp_err_t err;

  // Subsystems initialization routines.

  // Create event loop.
  err = esp_event_loop_create_default();
  ESP_GOTO_ON_ERROR(err, exit_err, "EVENT_LOOP",
                    "Can not create default event loop");

  // Init NVS (used for WiFi).
  err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NEW_VERSION_FOUND ||
      err == ESP_ERR_NVS_NO_FREE_PAGES) {
    err = nvs_flash_erase();
    ESP_GOTO_ON_ERROR(err, exit_err, "NVS",
                      "Can not format/erase NVS partition");

    err = nvs_flash_init();
    ESP_GOTO_ON_ERROR(err, exit_err, "NVS", "Can not init NVS flash");
  } else {
    ESP_GOTO_ON_ERROR(err, exit_err, "NVS", "Can not init NVS flash");
  }

  // Init network interfaces support.
  err = esp_netif_init();
  ESP_GOTO_ON_ERROR(err, exit_err, "NETIF", "Can not init NETIF TCP/IP stack");
  // End subsystems initialization routines.

  // Configure LED pin - blinker.
  gpio_config_t cfg = {
      .mode = GPIO_MODE_OUTPUT,
      .pin_bit_mask = GPIO_PIN_BIT(LED_PIN),
  };

  err = gpio_config(&cfg);
  ESP_GOTO_ON_ERROR(err, exit_err, "GPIO_LED", "Configuration failed");

  pthread_t led_thread;
  pthread_attr_t led_th_attr;
  ret = pthread_attr_init(&led_th_attr);
  if (ret != 0) {
    ESP_GOTO_ON_ERROR(ret, exit_err, "LED_THREAD",
                      "Thread attributes set error");
  }

  pthread_create(&led_thread, &led_th_attr, blink_led, NULL);

  err = control_uart_configure();
  ESP_GOTO_ON_ERROR(err, exit_err, "Control UART",
                    "Can not configure UART control port");

  err = camera_configure();
  ESP_GOTO_ON_ERROR(err, exit_err, "Camera", "Can not init Camera");

  err = wifi_init();
  ESP_GOTO_ON_ERROR(err, exit_err, WIFI_LOG_TAG, "Can not init WiFi");

  err = httpd_init(5, 6, 10, control_send_command);
  ESP_GOTO_ON_ERROR(err, exit_err, HTTPD_LOG_TAG, "Can not init httpd");

  // Set callback to handle car's messages.
  err = control_set_status_callback(car_report_cb, NULL);
  ESP_GOTO_ON_ERROR(err, exit_err, CONTROL_STREAM_LOG_TAG,
                    "Can not set control stream car callback");

  ESP_LOGI("Main", "Configuration completed");

  return;

exit_err:;
  char buf[512] = {0};
  ESP_LOGE("Fatal", "System stop, error: %s",
           esp_err_to_name_r(ret, buf, sizeof(buf)));

  abort();
}
