#include "wifi.h"
#include "esp_wifi.h"
#include <esp_check.h>
#include <esp_log.h>
#include <freertos/event_groups.h>
#include <string.h>

#include "nvs_flash.h"

#include "sdkconfig.h"

#define ERRBUF_MAXLEN (128)

// NVS keys for WiFi.
#define WIFI_NVS_NS "wifi_wD9oa9z"
#define WIFI_NVS_MODE "mode"
#define WIFI_NVS_SSID "ssid"
#define WIFI_NVS_PASSWORD "password"

// WiFi handlers.
static EventGroupHandle_t s_wifi_event_group;

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

esp_err_t wifi_validate_ssid(const char *ssid) {
  size_t len = strnlen(ssid, WIFI_SSID_MAXLEN + 1);
  if (len > WIFI_SSID_MAXLEN || len < WIFI_SSID_MINLEN) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, WIFI_LOG_TAG,
                        "SSID length is not in [%i, %i] range: %u",
                        WIFI_SSID_MINLEN, WIFI_SSID_MAXLEN, len);
  }

  return ESP_OK;
}

esp_err_t wifi_validate_password(const char *password) {
  size_t len = strnlen(password, WIFI_SSID_MAXLEN + 1);
  if (len > WIFI_SSID_MAXLEN || len < WIFI_SSID_MINLEN) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, WIFI_LOG_TAG,
                        "Password length is not in [%i, %i] range: %u",
                        WIFI_PASSWORD_MINLEN, WIFI_PASSWORD_MAXLEN, len);
  }

  return ESP_OK;
}

esp_err_t wifi_validate_mode(const enum wifi_mode mode) {
  switch (mode) {
  case wifi_mode_ap:
    return ESP_OK;
  case wifi_mode_st:
    return ESP_OK;
  }

  ESP_LOGE(WIFI_LOG_TAG, "WiFi mode %d is not supported", mode);

  return ESP_FAIL;
}

static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data) {
  if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
    esp_err_t err = esp_wifi_connect();
    if (err != ESP_OK) {
      char buf[256];
      ESP_LOGE(WIFI_LOG_TAG, "Can not connect to WiFi: %s",
               esp_err_to_name_r(err, buf, sizeof(buf)));
    } else {
      ESP_LOGI(WIFI_LOG_TAG, "Station started");
    }
  } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    ESP_LOGI(WIFI_LOG_TAG, "Got IP:" IPSTR, IP2STR(&event->ip_info.ip));
    xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
  }
}

static esp_err_t wifi_configure_station(const char *ssid, const char *password,
                                        bool use_nvs) {
  // Configure WiFi.
  esp_err_t err = ESP_OK;

  err = wifi_validate_ssid(ssid);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Provided SSID is not valid %d", err);

  err = wifi_validate_password(password);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Provided password is not valid %d",
                      err);

  s_wifi_event_group = xEventGroupCreate();
  /* Register Event handler */
  ESP_RETURN_ON_ERROR(
      esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID,
                                          &wifi_event_handler, NULL, NULL),
      WIFI_LOG_TAG, "Can not register event handler");
  ESP_RETURN_ON_ERROR(
      esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP,
                                          &wifi_event_handler, NULL, NULL),
      WIFI_LOG_TAG, "Can not register event handler");

  ESP_LOGI("WIFI", "Starting WiFi module in station mode");
  wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();

  if (!use_nvs) {
    wifi_init_config.nvs_enable = !WIFI_NVS_ENABLED;
  }

  err = esp_wifi_init(&wifi_init_config);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not init");

  err = esp_wifi_set_mode(WIFI_MODE_STA);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set mode station");

  wifi_sta_config_t wifi_sta_conf = {0};

  // Just a stub to make compiler strncpy output may be truncated copying
  // error go away.
  if (strlen(ssid) > WIFI_SSID_MAXLEN - 1) {
    ESP_LOGE(WIFI_LOG_TAG, "Must never be reached");
    return ESP_FAIL;
  }
  if (strlen(password) > WIFI_PASSWORD_MAXLEN - 1) {
    ESP_LOGE(WIFI_LOG_TAG, "Must never be reached");
    return ESP_FAIL;
  }
  strncpy((char *)wifi_sta_conf.ssid, ssid, sizeof(wifi_sta_conf.ssid));
  strncpy((char *)wifi_sta_conf.password, password,
          sizeof(wifi_sta_conf.password));

  wifi_config_t wifi_config = {.sta = wifi_sta_conf};

  err = esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not configure WiFi");

  err = esp_wifi_set_mode(WIFI_MODE_STA);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set mode station");

  // Configure IP stack for WiFi.
  esp_netif_t *sta_if = esp_netif_create_default_wifi_sta();
  err = esp_netif_set_default_netif(sta_if);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set network interface");

  err = esp_wifi_start();
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not start WiFi");

  ESP_LOGI(WIFI_LOG_TAG, "Hardware initialized, waiting for connection to AP");

  /*
   * Wait until either the connection is established (WIFI_CONNECTED_BIT) or
   * connection failed for the maximum number of re-tries (WIFI_FAIL_BIT).
   * The bits are set by event_handler() (see above)
   */
  EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                         WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                         pdFALSE, pdFALSE, portMAX_DELAY);

  /* xEventGroupWaitBits() returns the bits before the call returned,
   * hence we can test which event actually happened. */
  if (bits & WIFI_CONNECTED_BIT) {
    ESP_LOGI(WIFI_LOG_TAG, "Connected to ap SSID: %s", ssid);
  } else if (bits & WIFI_FAIL_BIT) {
    ESP_LOGI(WIFI_LOG_TAG, "Failed to connect to SSID: %s", ssid);
  } else {
    ESP_LOGE(WIFI_LOG_TAG, "UNEXPECTED EVENT");
    return ESP_FAIL;
  }
  // End WiFi configuration.

  return err;
}

static esp_err_t wifi_configure_ap(const char *ssid, const char *password,
                                   bool use_nvs) {
  // Configure WiFi.
  esp_err_t err = ESP_OK;

  err = wifi_validate_ssid(ssid);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Provided SSID is invalid %d", err);

  err = wifi_validate_password(password);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Provided password is invalid %d",
                      err);

  ESP_LOGI("WIFI", "Starting WiFi module in AP mode");
  wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();

  if (!use_nvs) {
    wifi_init_config.nvs_enable = !WIFI_NVS_ENABLED;
  }

  err = esp_wifi_init(&wifi_init_config);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not init");

  err = esp_wifi_set_mode(WIFI_MODE_AP);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set mode AP");

  wifi_ap_config_t wifi_ap_conf = {0};

  wifi_ap_conf.max_connection = WIFI_AP_MAX_CONNECTIONS;
  wifi_ap_conf.authmode = WIFI_AUTH_WPA2_PSK;

  // Just a stub to make compiler strncpy output may be truncated copying
  // error go away.
  if (strlen(ssid) > WIFI_SSID_MAXLEN - 1) {
    ESP_LOGE(WIFI_LOG_TAG, "Must never be reached");
    return ESP_FAIL;
  }
  if (strlen(password) > WIFI_PASSWORD_MAXLEN - 1) {
    ESP_LOGE(WIFI_LOG_TAG, "Must never be reached");
    return ESP_FAIL;
  }

  strncpy((char *)wifi_ap_conf.ssid, ssid, WIFI_SSID_MAXLEN);
  strncpy((char *)wifi_ap_conf.password, password, WIFI_PASSWORD_MAXLEN);

  wifi_config_t wifi_config = {.ap = wifi_ap_conf};

  err = esp_wifi_set_config(WIFI_IF_AP, &wifi_config);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not configure WiFi");

  err = esp_wifi_set_mode(WIFI_MODE_AP);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set mode access point");

  // Configure IP stack for WiFi.
  esp_netif_t *ap_if = esp_netif_create_default_wifi_ap();
  err = esp_netif_set_default_netif(ap_if);
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not set network interface");

  err = esp_wifi_start();
  ESP_RETURN_ON_ERROR(err, WIFI_LOG_TAG, "Can not start WiFi");

  ESP_LOGI(WIFI_LOG_TAG, "AP initialized, SSID: %s", ssid);

  return err;
}

#ifdef CONFIG_WIFI_DEFAULT_MODE_STATION
esp_err_t wifi_init_default() {
  // Default Station mode.
  ESP_LOGI(WIFI_LOG_TAG, "No NVS configuration: starting default station.");
  return wifi_configure_station(CONFIG_WIFI_DEFAULT_SSID,
                                CONFIG_WIFI_DEFAULT_PASSWORD, true);
}
#endif

#ifdef CONFIG_WIFI_DEFAULT_MODE_ACCESS_POINT
esp_err_t wifi_init_default() {
  // Default AP mode.
  ESP_LOGI(WIFI_LOG_TAG,
           "No NVS configuration: starting default access point.");
  // return wifi_configure_ap(default_ssid, default_password, true);
  // ToDo: revert to configure ap. Current is for development.
  return wifi_configure_ap(CONFIG_WIFI_DEFAULT_SSID,
                           CONFIG_WIFI_DEFAULT_PASSWORD, true);
}
#endif

esp_err_t wifi_init() {
  nvs_handle_t out_handle;
  char err_buf[ERRBUF_MAXLEN] = {0};

  esp_err_t ret = nvs_open(WIFI_NVS_NS, NVS_READONLY, &out_handle);
  if (ret == ESP_OK) {
    // Load configuration.
    uint8_t mode;
    ret = nvs_get_u8(out_handle, WIFI_NVS_MODE, &mode);
    ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                      "Can not get WiFi mode from NVS: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

    char ssid[WIFI_SSID_MAXLEN + 1] = {0};
    size_t len = WIFI_SSID_MAXLEN;
    ret = nvs_get_str(out_handle, WIFI_NVS_SSID, ssid, &len);
    ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                      "Can not get SSID from NVS: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

    char password[WIFI_PASSWORD_MAXLEN] = {0};
    len = WIFI_PASSWORD_MAXLEN;
    ret = nvs_get_str(out_handle, WIFI_NVS_PASSWORD, password, &len);
    ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                      "Can not get password from NVS: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

    switch (mode) {
    case wifi_mode_ap:
      ret = wifi_configure_ap(ssid, password, true);
      ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                        "can not configure AP: %s",
                        esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

      break;

    case wifi_mode_st:
      ret = wifi_configure_station(ssid, password, true);
      ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                        "can not configure station: %s",
                        esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

      break;

    default:
      ret = ESP_FAIL;
      ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG " wifi_configure:",
                        "unsupported WiFi mode: %d", mode);
    }

  close_nvs:
    nvs_close(out_handle);
    return ret;

  } else {
    return wifi_init_default();
  }

  ESP_LOGE(WIFI_LOG_TAG, "This line should never be reached");
  return ESP_FAIL;
}

esp_err_t wifi_set_persistent_config(const char *ssid, const char *password,
                                     const enum wifi_mode mode) {
  char err_buf[ERRBUF_MAXLEN] = {0};
  esp_err_t ret = ESP_OK;

  ret = wifi_validate_ssid(ssid);
  ESP_RETURN_ON_ERROR(ret, WIFI_LOG_TAG ": wifi_set_persistent_config",
                      "SSID length is not correct");

  ret = wifi_validate_password(password);
  ESP_RETURN_ON_ERROR(ret, WIFI_LOG_TAG ": wifi_set_persistent_config",
                      "Password length is not correct");

  ret = wifi_validate_mode(mode);
  ESP_RETURN_ON_ERROR(ret, WIFI_LOG_TAG ": wifi_set_persistent_config",
                      "WiFi mode is not correct");

  nvs_handle_t out_handle;
  ret = nvs_open(WIFI_NVS_NS, NVS_READWRITE, &out_handle);
  ESP_RETURN_ON_ERROR(ret, WIFI_LOG_TAG ": wifi_set_persistent_config",
                      "Can not open NVS storage: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret = nvs_set_u8(out_handle, WIFI_NVS_MODE, mode);
  ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG ": wifi_set_persistent_config",
                    "Can not write WiFi mode: %s",
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret = nvs_set_str(out_handle, WIFI_NVS_SSID, ssid);
  ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG ": wifi_set_persistent_config",
                    "Can not write WiFi SSID: %s",
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret = nvs_set_str(out_handle, WIFI_NVS_PASSWORD, password);
  ESP_GOTO_ON_ERROR(ret, close_nvs, WIFI_LOG_TAG ": wifi_set_persistent_config",
                    "Can not write WiFi password: %s",
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

close_nvs:
  nvs_close(out_handle);
  return ret;
}