#ifndef __WIFI_H
#define __WIFI_H

#include "esp_err.h"
#include <stdbool.h>

#define WIFI_LOG_TAG "WIFI"
#define WIFI_SSID_MAXLEN (32)
#define WIFI_SSID_MINLEN (8)
#define WIFI_PASSWORD_MAXLEN (64)
#define WIFI_PASSWORD_MINLEN (8)
#define WIFI_AP_MAX_CONNECTIONS (4)

// WiFi modes
enum wifi_mode {
  wifi_mode_ap = 1,
  wifi_mode_st = 2,
};

// Requires netif to be initialized, sets the WiFi interface as default.
esp_err_t wifi_init();
esp_err_t wifi_set_persistent_config(const char *ssid, const char *password,
                                     const enum wifi_mode mode);

esp_err_t wifi_validate_ssid(const char *ssid);
esp_err_t wifi_validate_password(const char *password);
esp_err_t wifi_validate_mode(const enum wifi_mode mode);
#endif