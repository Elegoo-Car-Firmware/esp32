#ifndef __CONTROL_STREAM
#define __CONTROL_STREAM

#include <stdatomic.h>

#include "driver/uart.h"
#include "esp_err.h"
#include "esp_http_server.h"

#define CONTROL_STREAM_LOG_TAG "CONTROL"

// struct control_arg {
//   httpd_req_t *req;
//   atomic_bool is_running;
// };

struct control_uart_config {
  int tx_io_num;
  int rx_io_num;
  uart_port_t uart_num;
  uart_config_t uart_config;
};

enum control_serial_command_type {
  serial_command_type_action = 1,
  serial_command_type_request = 2,
};

enum control_car_request {
  get_battery = 1,
  get_sonar = 2,
  get_accelerator = 3,
};

enum control_car_action {
  configure_time = 1,

  mov_stop = 2,
  mov_forward = 3,
  mov_back = 4,
  mov_left = 5,
  mov_right = 6,

  speed_decrease = 7,
  speed_increase = 8,
  speed_reset = 9,
  speed_set = 10,

  mov_duration_increase = 11,
  mov_duration_decrease = 12,
  mov_duration_reset = 13,
  mov_duration_set = 14,

  servo_left = 15,
  servo_right = 16,
  servo_set_angle = 17,
};

struct control_serial_command {
  enum control_serial_command_type type;
  union {
    enum control_car_action action;
    enum control_car_request request;
  } command;
  uint32_t value; // To fit timestamps
};

enum control_car_message_type {
  control_car_message_type_report = 1,
  control_car_message_type_reset = 2,
};

struct control_status_report {
  enum control_car_message_type msg_type;
  double battery_voltage;
  uint8_t servo_angle;
  uint8_t car_speed;
  uint16_t sonar_distance;
  uint32_t uptime;
};

esp_err_t control_configure_uart(struct control_uart_config *config);
esp_err_t control_load_json(const char *buf, const size_t buflen,
                            struct control_serial_command *result);
esp_err_t control_send_command(const struct control_serial_command *command);
esp_err_t control_set_status_callback(
    esp_err_t cb(struct control_status_report *status, void *user_context),
    void *user_context);

#endif