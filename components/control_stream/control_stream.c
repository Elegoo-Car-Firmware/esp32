#include "control_stream.h"

#include <errno.h>
#include <stdint.h>

#include "esp_check.h"
#include "esp_log.h"

#include "cJSON/cJSON.h"

#define ERRBUF_MAXLEN (128)

// Limited by Arduino serial buffer size.
#define UART_SEND_MAXLEN (64)
#define UART_QUEUE_SIZE (4)

#define UART_TXRX_BUF_LEN (256) // Must be more than UART_HW_FIFO_LEN.
const size_t uart_queue_len = 2;

static QueueHandle_t uart_events;
static uart_port_t UART_PORT = -1;

#define CAR_STATUS_REPORT_MAXLEN (UART_TXRX_BUF_LEN)
#define CAR_STATUS_CALLBACK_STACK (4096)
#define CAR_STATUS_CALLBACK_WAIT_MS (1000)
#define CAR_STATUS_CALLBACK_PRIO (1)
#define CAR_STATUS_PATTERN '\n'
#define CAR_STATUS_PATTERN_COUNT (1)

static bool car_status_stream_running = false;
static esp_err_t (*car_status_callback)(struct control_status_report *status,
                                        void *user_context) = NULL;

esp_err_t control_configure_uart(struct control_uart_config *config) {
  esp_err_t ret = ESP_OK;
  char err_buf[ERRBUF_MAXLEN + 1] = {0};

  UART_PORT = config->uart_num;

  ret = uart_param_config(config->uart_num, &config->uart_config);
  ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                    "Can not configure UART port %d: %s", config->uart_num,
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret = uart_set_pin(config->uart_num, config->tx_io_num, config->rx_io_num, -1,
                     -1);
  ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                    "Can not configure UART pins: UART=%d, tx=%d, rx=%d: %s",
                    config->uart_num, config->tx_io_num, config->rx_io_num,
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret =
      uart_driver_install(config->uart_num, UART_TXRX_BUF_LEN,
                          UART_TXRX_BUF_LEN, UART_QUEUE_SIZE, &uart_events, 0);
  ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                    "Can not configure UART driver: UART=%d: %s",
                    config->uart_num,
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  ret = uart_set_mode(config->uart_num, UART_MODE_UART);
  ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                    "Can not configure UART mode: UART=%d: %s",
                    config->uart_num,
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

cleanup:
  return ret;
}

esp_err_t control_load_json(const char *buf, const size_t buflen,
                            struct control_serial_command *result) {
  esp_err_t ret = ESP_OK;

  // Load client's request.
  cJSON *command_json = cJSON_ParseWithLength(buf, buflen);
  if (command_json == NULL) {
    ESP_GOTO_ON_ERROR(ESP_FAIL, ret_err, CONTROL_STREAM_LOG_TAG,
                      "Can not parse command from client: %s", buf);
  }

  // Command type.
  cJSON *command_type_json =
      cJSON_GetObjectItemCaseSensitive(command_json, "type");
  if (command_type_json == NULL) {
    ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Client command does not contain \"type\" field: %s",
                      buf);
  }
  enum control_serial_command_type cmd_type =
      (enum control_serial_command_type)cJSON_GetNumberValue(command_type_json);

  switch (cmd_type) {
  case serial_command_type_action:
    result->type = serial_command_type_action;

    // Command action value.
    cJSON *cmd_value_json =
        cJSON_GetObjectItemCaseSensitive(command_json, "value");
    if (cmd_value_json == NULL) {
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"action\" does not contain "
                        "\"value\" field: %s",
                        buf);
    }

    double value_tmp = cJSON_GetNumberValue(cmd_value_json);
    if (value_tmp < 0) {
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"action\" "
                        "\"value\" field is negative");
    }
    if (value_tmp > UINT32_MAX) {
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"action\" "
                        "\"value\" field is larger than max uint32");
    }
    result->value = (uint32_t)value_tmp;

    // Command action.
    cJSON *cmd_action_json =
        cJSON_GetObjectItemCaseSensitive(command_json, "action");
    if (cmd_action_json == NULL) {
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"action\" does not contain "
                        "\"action\" field: %s",
                        buf);
    }

    enum control_car_action cmd_action = cJSON_GetNumberValue(cmd_action_json);
    switch (cmd_action) {
    case configure_time:
      ESP_LOGI(CONTROL_STREAM_LOG_TAG, "Got configure_time command action");
      break;

    case mov_stop:
      result->command.action = mov_stop;
      break;
    case mov_back:
      result->command.action = mov_back;
      break;
    case mov_forward:
      result->command.action = mov_forward;
      break;
    case mov_left:
      result->command.action = mov_left;
      break;
    case mov_right:
      result->command.action = mov_right;
      break;

    case speed_decrease:
      result->command.action = speed_decrease;
      break;
    case speed_increase:
      result->command.action = speed_increase;
      break;
    case speed_reset:
      result->command.action = speed_reset;
      break;
    case speed_set:
      result->command.action = speed_set;
      break;

    case mov_duration_decrease:
      result->command.action = mov_duration_decrease;
      break;
    case mov_duration_increase:
      result->command.action = mov_duration_increase;
      break;
    case mov_duration_set:
      result->command.action = mov_duration_set;
      break;
    case mov_duration_reset:
      result->command.action = mov_duration_reset;
      break;

    case servo_left:
      result->command.action = servo_left;
      break;
    case servo_right:
      result->command.action = servo_right;
      break;
    case servo_set_angle:
      result->command.action = servo_set_angle;
      break;

    default:
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"action\" has unsupported "
                        "control_car_action: %d",
                        cmd_action);
    }

    break;

  case serial_command_type_request:
    result->type = serial_command_type_request;

    // Get request.
    cJSON *request_json =
        cJSON_GetObjectItemCaseSensitive(command_json, "request");
    if (request_json == NULL) {
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"reqiest\" does not contain "
                        "\"request\" field: %s",
                        buf);
    }

    enum control_car_request request_raw = cJSON_GetNumberValue(request_json);

    switch (request_raw) {
    case get_battery:
      result->command.request = get_battery;
      break;
    case get_sonar:
      result->command.request = get_sonar;
      break;
    case get_accelerator:
      result->command.request = get_accelerator;
      break;

    default:
      ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                        "Client command of type \"request\" has unsupported "
                        "control_car_request: %d",
                        request_raw);
    }

    result->value = 0;

    break;

  default:
    ESP_GOTO_ON_ERROR(ESP_FAIL, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Client command type is not supported, value is %d",
                      cmd_type);
  }

cleanup:
  cJSON_Delete(command_type_json);
ret_err:
  return ret;
}

esp_err_t control_send_command(const struct control_serial_command *command) {
  char buf[UART_SEND_MAXLEN] = {0};
  int len = 0;
  esp_err_t ret = ESP_OK;

  switch (command->type) {
  case serial_command_type_action:
    // Clang incorrectly marks %lu as error.
    len = snprintf(
        buf, UART_SEND_MAXLEN, "{\"type\":%d,\"action\":%d,\"value\":%lu}\r\n",
        serial_command_type_action, command->command.action, command->value);
    break;

  case serial_command_type_request:;
    len = snprintf(buf, UART_SEND_MAXLEN, "{\"type\":%d,\"request\":%d}\r\n",
                   serial_command_type_request, command->command.request);
    break;

  default:
    return -1;
  }

  if (len < 0 || len > UART_SEND_MAXLEN) {
    ESP_RETURN_ON_ERROR(
        ESP_FAIL, CONTROL_STREAM_LOG_TAG,
        "Not enough space to serialize command, capacity: %d, needed space: %d",
        UART_SEND_MAXLEN, len);
  }

  int sent = uart_write_bytes(UART_PORT, buf, len);
  if (sent < 0) {
    ret = ESP_FAIL;
    ESP_RETURN_ON_ERROR(ret, CONTROL_STREAM_LOG_TAG,
                        "UART send parameter error");
  }
  if (sent < len) {
    ESP_LOGW(CONTROL_STREAM_LOG_TAG,
             "UART: not all data sent: expected %d, sent %d", len, sent);
  }

  return ret;
}

static esp_err_t json_get_msg_type(const cJSON *object, const char *field_name,
                                   enum control_car_message_type *value) {
  cJSON *field_value = cJSON_GetObjectItemCaseSensitive(object, field_name);
  if (field_value == NULL) {
    ESP_RETURN_ON_ERROR(
        ESP_FAIL, CONTROL_STREAM_LOG_TAG,
        "json_get_msg_type: JSON does not contain \"%s\" field.", field_name);
  } else {
    if (cJSON_IsNumber(field_value)) {
      int tmp = (int)cJSON_GetNumberValue(field_value);

      switch (tmp) {
      case control_car_message_type_report:
        *value = control_car_message_type_report;
        break;
      case control_car_message_type_reset:
        *value = control_car_message_type_reset;
        break;

      default:
        ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                            "json_get_msg_type: %d is not supported value",
                            tmp);
      }

    } else {
      ESP_RETURN_ON_ERROR(
          ESP_FAIL, CONTROL_STREAM_LOG_TAG,
          "json_get_msg_type: JSON \"%s\" field has unexpected type",
          field_name);
    }
  }

  return ESP_OK;
}

esp_err_t json_get_double(const cJSON *object, const char *field_name,
                          double *value) {
  cJSON *field_value = cJSON_GetObjectItemCaseSensitive(object, field_name);
  if (field_value == NULL) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                        "JSON does not contain \"%s\" field.", field_name);
  } else {
    if (cJSON_IsNumber(field_value)) {
      *value = cJSON_GetNumberValue(field_value);
    } else {
      ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                          "JSON \"%s\" field has unexpected type", field_name);
    }
  }

  return ESP_OK;
}

static esp_err_t json_get_uint8_t(const cJSON *object, const char *field_name,
                                  uint8_t *value) {
  cJSON *field_value = cJSON_GetObjectItemCaseSensitive(object, field_name);
  if (field_value == NULL) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                        "JSON does not contain \"%s\" field.", field_name);
  } else {
    if (cJSON_IsNumber(field_value)) {
      *value = cJSON_GetNumberValue(field_value);
    } else {
      ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                          "JSON \"%s\" field has unexpected type", field_name);
    }
  }

  return ESP_OK;
}

static esp_err_t json_get_uint16_t(const cJSON *object, const char *field_name,
                                   uint16_t *value) {
  cJSON *field_value = cJSON_GetObjectItemCaseSensitive(object, field_name);
  if (field_value == NULL) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                        "JSON does not contain \"%s\" field.", field_name);
  } else {
    if (cJSON_IsNumber(field_value)) {
      *value = cJSON_GetNumberValue(field_value);
    } else {
      ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                          "JSON \"%s\" field has unexpected type", field_name);
    }
  }

  return ESP_OK;
}

static esp_err_t json_get_uint32_t(const cJSON *object, const char *field_name,
                                   uint32_t *value) {
  cJSON *field_value = cJSON_GetObjectItemCaseSensitive(object, field_name);
  if (field_value == NULL) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                        "JSON does not contain \"%s\" field.", field_name);
  } else {
    if (cJSON_IsNumber(field_value)) {
      *value = cJSON_GetNumberValue(field_value);
    } else {
      ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_STREAM_LOG_TAG,
                          "JSON \"%s\" field has unexpected type", field_name);
    }
  }

  return ESP_OK;
}

static esp_err_t control_parse_status(const char *buf, size_t buflen,
                                      struct control_status_report *status) {

  // Expect messages like:
  // {"battery":4.68585,"servo":90,"speed":100,"timestamp":511456, "type": 1|2}
  ESP_LOGD(CONTROL_STREAM_LOG_TAG,
           "control_parse_status: parsing message %s with length %u", buf,
           buflen);

  cJSON *status_json = cJSON_ParseWithLength(buf, buflen);
  if (status_json == NULL) {
    const char *issue_ptr = cJSON_GetErrorPtr();

    ESP_LOGE(CONTROL_STREAM_LOG_TAG,
             "Can not parse JSON status, error at char %c, position: %u",
             *issue_ptr, issue_ptr - buf);
    return ESP_FAIL;
  }

  esp_err_t ret = ESP_OK;

  // Read fields.
  ret = json_get_msg_type(status_json, "type", &(status->msg_type));
  ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                    "Failure to get double value from \"type\" field");

  if (status->msg_type == control_car_message_type_report) {
    ret = json_get_double(status_json, "battery", &(status->battery_voltage));
    ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Failure to get double value from \"battery\" field");

    ret = json_get_uint8_t(status_json, "servo", &(status->servo_angle));
    ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Failure to get uint8_t value from \"servo\" field");

    ret = json_get_uint16_t(status_json, "sonar", &(status->sonar_distance));
    ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Failure to get uint16_t value from \"sonar\" field");

    ret = json_get_uint8_t(status_json, "speed", &(status->car_speed));
    ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Failure to get uint8_t value from \"speed\" field");

    ret = json_get_uint32_t(status_json, "timestamp", &(status->uptime));
    ESP_GOTO_ON_ERROR(ret, cleanup, CONTROL_STREAM_LOG_TAG,
                      "Failure to get uint32_t value from \"timestamp\" field");
  }

cleanup:
  cJSON_Delete(status_json);
  return ret;
}

void control_get_status(void *user_context) {
  ESP_LOGI(CONTROL_STREAM_LOG_TAG, "Starting car stream status loop");

  uart_event_t event;
  esp_err_t ret;
  char err_buf[ERRBUF_MAXLEN + 1] = {0};

  // Read events until timeout or the pattern (end of line) match.
  while (car_status_stream_running) {
    if (!xQueueReceive(uart_events, &event,
                       pdMS_TO_TICKS(CAR_STATUS_CALLBACK_WAIT_MS))) {
      continue;
    }

    ESP_LOGD(CONTROL_STREAM_LOG_TAG, "Received event, type %d", event.type);

    if (event.type == UART_PATTERN_DET) {
      size_t buffered_size;
      ret = uart_get_buffered_data_len(UART_PORT, &buffered_size);
      ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                        "Can not get UART buffered data len: %s",
                        esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

      int pos = uart_pattern_pop_pos(UART_PORT);
      ESP_LOGD(CONTROL_STREAM_LOG_TAG,
               "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos,
               buffered_size);

      if (pos >= CAR_STATUS_REPORT_MAXLEN) {
        ESP_LOGE(CONTROL_STREAM_LOG_TAG,
                 "UART buffered data len is larger than %d",
                 CAR_STATUS_REPORT_MAXLEN);
        ret = uart_flush_input(UART_PORT);
        ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                          "Can not flush UART buffered data len: %s",
                          esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
        goto skip_loop;
      }

      if (pos == -1) {
        // There used to be a UART_PATTERN_DET event, but the pattern position
        // queue is full so that it can not record the position. We should set a
        // larger queue size. As an example, we directly flush the rx buffer
        // here.
        ESP_LOGW(CONTROL_STREAM_LOG_TAG,
                 "UART event queue is full, cannot fit the pattern's position, "
                 "flushing the queue");
        ret = uart_flush_input(UART_PORT);
        ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                          "Can not flush UART event queue: %s",
                          esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
      } else {
        char msg[CAR_STATUS_REPORT_MAXLEN + 1] = {0};
        size_t msg_len;
        int read_bytes;
        const int read_tw_ms =
            100; // Assuming that the bytes are already in UART
                 // buffer, so 100 ms is more than enough.

        read_bytes =
            uart_read_bytes(UART_PORT, msg, pos, pdMS_TO_TICKS(read_tw_ms));
        if (read_bytes == -1) {
          ret = ESP_FAIL;
          ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                            "Can not read UART car status message: %s",
                            esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
        }
        msg_len = read_bytes;

        uint8_t pat[CAR_STATUS_PATTERN_COUNT + 1] = {0};

        read_bytes = uart_read_bytes(UART_PORT, pat, CAR_STATUS_PATTERN_COUNT,
                                     pdMS_TO_TICKS(read_tw_ms));
        if (read_bytes == -1) {
          ret = ESP_FAIL;
          ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                            "Can not read UART pattern bytes: %s",
                            esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
        }

        ESP_LOGD(CONTROL_STREAM_LOG_TAG, "read data: %s", msg);

        struct control_status_report status;
        ret = control_parse_status(msg, msg_len, &status);
        ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                          "Can not parse car status: %s, message: '%s'",
                          esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN), msg);

        if (car_status_callback == NULL) {
          ret = ESP_FAIL;
          ESP_GOTO_ON_ERROR(
              ret, skip_loop, CONTROL_STREAM_LOG_TAG,
              "Error in status callback: callback function is NULL");
        }

        ret = car_status_callback(&status, user_context);
        ESP_GOTO_ON_ERROR(ret, skip_loop, CONTROL_STREAM_LOG_TAG,
                          "Error in status callback: %s",
                          esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
      }
    }

  skip_loop:;
  }

  ESP_LOGI(CONTROL_STREAM_LOG_TAG, "Stopping car stream status loop");
  vTaskDelete(NULL);
}

esp_err_t control_set_status_callback(
    esp_err_t cb(struct control_status_report *status, void *user_context),
    void *user_context) {
  esp_err_t ret = ESP_OK;
  char err_buf[ERRBUF_MAXLEN + 1] = {0};

  // Stop processing.
  car_status_stream_running = false;

  ret = uart_disable_pattern_det_intr(UART_PORT);
  ESP_RETURN_ON_ERROR(
      ret, CONTROL_STREAM_LOG_TAG,
      "Can not disable UART end of line pattern interrupt: UART=%d: %s",
      UART_PORT, esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  // Change callback.
  car_status_callback = cb;

  car_status_stream_running = true;
  ret = xTaskCreate(control_get_status, "Car Status Stream",
                    CAR_STATUS_CALLBACK_STACK, user_context,
                    CAR_STATUS_CALLBACK_PRIO, NULL);
  if (ret != pdPASS) {
    ret = ESP_ERR_NO_MEM;
    ESP_RETURN_ON_ERROR(ret, CONTROL_STREAM_LOG_TAG,
                        "Can not start car status handler task: %s",
                        esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
  }

  // Reenable processing.
  // To be honest, I have no idea why the number "9" works and the
  // ESP32 documentation:
  // https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/uart.html#_CPPv433uart_enable_pattern_det_baud_intr11uart_port_tc7uint8_tiii
  // did not help me much.
  ret = uart_enable_pattern_det_baud_intr(UART_PORT, CAR_STATUS_PATTERN,
                                          CAR_STATUS_PATTERN_COUNT, 9, 0, 0);
  ESP_RETURN_ON_ERROR(
      ret, CONTROL_STREAM_LOG_TAG,
      "Can not configure UART end of line pattern interrupt: UART=%d: %s",
      UART_PORT, esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  return ret;
}
