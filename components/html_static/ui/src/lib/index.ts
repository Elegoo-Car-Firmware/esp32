// place files you want to import through the `$lib` alias in this folder.
export const videoURL = '/stream/img';
export const stubVideoURL = '/icons/camera-video.svg';
export const wsURLPath = '/stream/control';
export const configureWiFiURL = '/configure/wifi';
export const rebootURL = '/reboot';
