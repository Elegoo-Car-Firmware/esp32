import { sveltekit } from '@sveltejs/kit/vite';
import pure from 'purecss';
import copy from 'rollup-plugin-copy';
import { defineConfig } from 'vite';

console.log(pure.getFilePath('pure-min.css'));

export default defineConfig({
	plugins: [
		copy({
			targets: [
				{ src: pure.getFilePath('pure-min.css'), dest: 'static/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-down.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-up.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-up-left.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-up-right.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-left.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/arrow-right.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/patch-plus.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/patch-minus.svg', dest: 'static/icons/' },
				{ src: 'node_modules/bootstrap-icons/icons/camera-video.svg', dest: 'static/icons/' },
				{
					src: 'node_modules/bootstrap-icons/icons/car-front.svg',
					dest: 'static/',
					rename: 'favicon.ico'
				}
			]
		}),
		sveltekit()
	],
	build: {
		assetsInlineLimit: 16536,
		cssCodeSplit: false
	}
});
