#ifndef __HTTPD_CAMERA_H
#define __HTTPD_CAMERA_H

#include "esp_check.h"
#include "esp_err.h"
#include "esp_http_server.h"
#include <stdint.h>

esp_err_t httpd_camera_register_handlers(httpd_handle_t httpd,
                                         const char *img_url,
                                         const char *img_stream);
esp_err_t httpd_camera_start_workers(uint8_t workers, unsigned int prio);

void httpd_camera_stop_workers();
#endif