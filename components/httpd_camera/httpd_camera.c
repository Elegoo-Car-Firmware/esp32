#include "httpd_camera.h"
#include "esp_camera.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_http_server.h"
#include "esp_log.h"
#include "freertos/projdefs.h"
#include <errno.h>
#include <stdatomic.h>

#include <freertos/FreeRTOS.h>

#define CAM_LOG_TAG "HTTPD_CAM"
#define WORK_QUEUE_LEN 2
#define WORKER_STACK_SIZE 3072

typedef esp_err_t (*httpd_req_handler_t)(httpd_req_t *req);

typedef struct {
  httpd_req_t *req;
  httpd_req_handler_t handler;
} httpd_async_req_t;

static SemaphoreHandle_t workers_count;
static QueueHandle_t work_queue;
static TaskHandle_t *active_workers;
atomic_bool workers_running;

static esp_err_t httpd_handler_image(httpd_req_t *req) {
  camera_fb_t *fb = esp_camera_fb_get();
  if (fb == NULL) {
    return httpd_resp_send_500(req);
  }

  esp_err_t err;
  esp_err_t ret = 0;

  err = httpd_resp_set_type(req, "image/jpeg");
  ESP_GOTO_ON_ERROR(err, cleanup, CAM_LOG_TAG, "Can not send image");

  err = httpd_resp_send(req, (char *)fb->buf, fb->len);
  ESP_GOTO_ON_ERROR(err, cleanup, CAM_LOG_TAG, "Can not send image");
cleanup:;
  esp_camera_fb_return(fb);
  return ret;
}

static httpd_uri_t httpd_uri_cam_jpeg = {
    .method = HTTP_GET,
    .handler = httpd_handler_image,
};

#define HTTPD_STREAM_BOUNDARY "108Z3qA1JrYMAuTyGzFkK9JTsXjGOBEU"
#define HTTPD_STREAM_TYPE                                                      \
  "multipart/x-mixed-replace; boundary=" HTTPD_STREAM_BOUNDARY
#define HTTPD_STREAM_DELIMETER                                                 \
  "\r\n--" HTTPD_STREAM_BOUNDARY "\r\n"                                        \
  "Content-type: image/jpeg\r\n"                                               \
  "Content-Length: %u\r\n\r\n"

static esp_err_t sync_http_handler_image_stream(httpd_req_t *req) {
  esp_err_t ret = ESP_OK;

  ESP_LOGI(CAM_LOG_TAG, "Starting Image stream");

  ret = httpd_resp_set_type(req, HTTPD_STREAM_TYPE);
  ESP_RETURN_ON_ERROR(ret, CAM_LOG_TAG, "Can not send stream type: %s",
                      HTTPD_STREAM_TYPE);

  while (atomic_load(&workers_running)) {
    camera_fb_t *fb = esp_camera_fb_get();
    if (fb == NULL) {
      ESP_LOGE(CAM_LOG_TAG, "Failure receiving camera framebuffer");
      goto cleanup;
    }

    // 4MB PSRAM means max 4194304 bytes of data, the length of 4194304
    // is 7, so 8 chars for content-length is enough.
    char delim_buf[sizeof(HTTPD_STREAM_DELIMETER) + 8] = {0};

    // Clangd language server incorrectly determines type of fb->len
    // so it marks the format string and the arguments as errors.
    size_t pl = snprintf(delim_buf, sizeof(HTTPD_STREAM_DELIMETER) + 8,
                         HTTPD_STREAM_DELIMETER, fb->len);

    if (pl > sizeof(delim_buf) / sizeof(delim_buf[0])) {
      // Clangd language server incorrectly determines type of size_t
      // so it marks the format string and the arguments as errors.
      ESP_GOTO_ON_ERROR(EMSGSIZE, cleanup, CAM_LOG_TAG,
                        "Content delimeter size is longer than the buffer: "
                        "buff len=%u, content len=%u",
                        sizeof(delim_buf), pl);
    }

    ret = httpd_resp_send_chunk(req, delim_buf, pl);
    ESP_GOTO_ON_ERROR(ret, cleanup, CAM_LOG_TAG,
                      "Can not send image headers: %s", delim_buf);

    ret = httpd_resp_send_chunk(req, (const char *)fb->buf, fb->len);
    ESP_GOTO_ON_ERROR(ret, cleanup, CAM_LOG_TAG, "Can not send image");

  cleanup:
    esp_camera_fb_return(fb);

    if (ret != ESP_OK) {
      httpd_resp_send_500(req);
      break;
    }
  }

  ESP_LOGI(CAM_LOG_TAG, "Stopped Image stream");

  return ret;
}

static esp_err_t send_no_workers(httpd_req_t *req) {
  esp_err_t ret = httpd_resp_set_status(req, "409 Conflict");
  ESP_GOTO_ON_ERROR(ret, exit_err, CAM_LOG_TAG, "Can not set HTTP status");

  ret = httpd_resp_set_type(req, HTTPD_TYPE_TEXT);
  ESP_GOTO_ON_ERROR(ret, exit_err, CAM_LOG_TAG,
                    "Can not set HTTP content type");

  ret = httpd_resp_sendstr(req, "No workers available");
  ESP_GOTO_ON_ERROR(ret, exit_err, CAM_LOG_TAG, "Can not send response");

  return ret;

exit_err:;
  char err_buf[128] = {0};
  ESP_LOGW(
      CAM_LOG_TAG, "Can not send error response to a client: %s",
      esp_err_to_name_r(ret, err_buf, sizeof(err_buf) / sizeof(err_buf[0])));

  return ret;
}

static esp_err_t async_http_handler_image_stream(httpd_req_t *req) {
  // must create a copy of the request that we own
  httpd_req_t *copy = NULL;
  httpd_async_req_t *async_req = NULL;

  esp_err_t ret;

  // Time to wait until a worker is available.
  const int wait_time_ms = 100;

  if (xSemaphoreTake(workers_count, pdMS_TO_TICKS(wait_time_ms)) == false) {
    send_no_workers(req);
    ret = ESP_FAIL;
    ESP_GOTO_ON_ERROR(ret, err_cleanup, CAM_LOG_TAG,
                      "No workers are available");
  }

  ret = httpd_req_async_handler_begin(req, &copy);
  ESP_GOTO_ON_ERROR(ret, err_cleanup, CAM_LOG_TAG,
                    "Can not allocate memory for HTTP request");

  async_req = malloc(sizeof(*async_req));
  if (async_req == NULL) {
    ret = ESP_ERR_NO_MEM;
    ESP_GOTO_ON_ERROR(ret, err_complete_cleanup, CAM_LOG_TAG,
                      "Can not allocate memory for httpd_async_req_t request");
  }

  // ESP_LOGI("DEBUG", "Request address %p", async_req);

  async_req->req = copy;
  async_req->handler = sync_http_handler_image_stream;

  // Since worker_ready_count > 0 the queue should already have space.
  // But lets wait a bit just to be safe.
  if (xQueueSend(work_queue, &async_req, pdMS_TO_TICKS(wait_time_ms)) ==
      false) {
    send_no_workers(req);
    ret = ESP_FAIL;
    ESP_GOTO_ON_ERROR(ret, err_complete_cleanup, CAM_LOG_TAG,
                      "Workers queue requests is full");
  }

  return ESP_OK;

err_complete_cleanup:
  httpd_req_async_handler_complete(copy); // cleanup

err_cleanup:
  free(async_req);
  return ret;
}

static httpd_uri_t httpd_uri_cam_jpeg_stream = {
    .method = HTTP_GET,
    .handler = async_http_handler_image_stream,
};

esp_err_t httpd_camera_register_handlers(httpd_handle_t httpd,
                                         const char *img_url,
                                         const char *img_stream) {
  httpd_uri_cam_jpeg_stream.uri = img_stream;
  httpd_uri_cam_jpeg.uri = img_url;

  esp_err_t err = httpd_register_uri_handler(httpd, &httpd_uri_cam_jpeg_stream);
  ESP_RETURN_ON_ERROR(err, CAM_LOG_TAG,
                      "Can not register camera MJPEG handler");

  err = httpd_register_uri_handler(httpd, &httpd_uri_cam_jpeg);
  ESP_RETURN_ON_ERROR(err, CAM_LOG_TAG, "Can not register camera JPEG handler");

  return err;
}

static void worker(void *arg) {
  const int poll_ival = 1000;

  ESP_LOGI(CAM_LOG_TAG, "Starting async worker, poll interval %d ms",
           poll_ival);
  char err_buf[128] = {0};

  while (atomic_load(&workers_running)) {
    esp_err_t err;

    httpd_async_req_t *async_req;
    if (xQueueReceive(work_queue, &async_req, pdMS_TO_TICKS(poll_ival))) {
      // ESP_LOGI("DEBUG", "Received Request address %p", async_req);

      err = async_req->handler(async_req->req); // Error is ignored by design.
      if (err != ESP_OK) {
        if (err != ESP_ERR_HTTPD_RESP_SEND) {
          ESP_LOGE(CAM_LOG_TAG, "Error handling request: %s",
                   esp_err_to_name_r(err, err_buf, sizeof(err_buf)));
        } else {
          ESP_LOGI(CAM_LOG_TAG, "Can not send response: %s",
                   esp_err_to_name_r(err, err_buf, sizeof(err_buf)));
        }
        goto cleanup;
      }

      err = httpd_req_async_handler_complete(async_req->req);
      if (err != ESP_OK) {
        ESP_LOGE(CAM_LOG_TAG, "Error completing request: %s",
                 esp_err_to_name_r(err, err_buf, sizeof(err_buf)));
        goto cleanup;
      }

    cleanup:
      free(async_req);

      xSemaphoreGive(workers_count);
    }
  }

  ESP_LOGI(CAM_LOG_TAG, "Stopping async worker");
}

void httpd_camera_stop_workers() {
  atomic_init(&workers_running, false);

  free(workers_count);
  free(work_queue);
  free(active_workers);

  workers_count = NULL;
  work_queue = NULL;
  active_workers = NULL;
}

esp_err_t httpd_camera_start_workers(uint8_t workers, unsigned int prio) {
  int error = ESP_OK;
  char err_buf[128] = {0};
  // Reset values.
  workers_count = NULL;
  work_queue = NULL;
  active_workers = NULL;

  workers_count = xSemaphoreCreateCounting(workers, workers);
  if (workers_count == NULL) {
    error = errno;
    ESP_LOGE(CAM_LOG_TAG,
             "Can not allocate memory for workers count semaphore: %s",
             esp_err_to_name_r(errno, err_buf, sizeof(err_buf)));
    goto err_cleanup;
  }

  work_queue = xQueueCreate(WORK_QUEUE_LEN, sizeof(httpd_async_req_t));
  if (work_queue == NULL) {
    error = errno;
    ESP_LOGE(CAM_LOG_TAG, "Can not allocate memory for work queue: %s",
             esp_err_to_name_r(errno, err_buf, sizeof(err_buf)));
    goto err_cleanup;
  }

  active_workers = calloc(workers, sizeof(TaskHandle_t));
  if (active_workers == NULL) {
    error = errno;
    ESP_LOGE(CAM_LOG_TAG,
             "Can not allocate memory for workers task handlers: %s",
             esp_err_to_name_r(errno, err_buf, sizeof(err_buf)));
    goto err_cleanup;
  }

  atomic_init(&workers_running, true);

  for (uint8_t i = 0; i < workers; i++) {
    char task_name[64] = {0};
    snprintf(task_name, sizeof(task_name), CAM_LOG_TAG "-%d", i);
    if (!xTaskCreate(worker, task_name, WORKER_STACK_SIZE, 0, prio,
                     &active_workers[i])) {
      ESP_LOGE(CAM_LOG_TAG, "Failed to create async worker task: %s",
               esp_err_to_name_r(errno, err_buf, sizeof(err_buf)));
      goto err_cleanup;
    }
  }

  return error;

err_cleanup:
  httpd_camera_stop_workers();
  return error;
}
