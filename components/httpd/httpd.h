#include "esp_err.h"

#include "control_stream.h"
#include "wifi.h"

#define HTTPD_LOG_TAG "HTTPD"
#define HTTPD_PATH_HEALTH "/health"
#define HTTPD_PATH_GET_IMG "/get/img"
#define HTTPD_PATH_STREAM_IMG "/stream/img"
#define HTTPD_PATH_INDEX_STREAM_IMG "/html/stream"
#define HTTPD_PATH_CONTROL_STREAM "/stream/control"
#define HTTPD_PATH_CONFIGURE_WIFI "/configure/wifi"
#define HTTPD_PATH_REBOOT "/reboot"

// Requires netif to be initialized.
esp_err_t
httpd_init(unsigned int httpd_prio, unsigned int camera_prio, int max_sockets,
           esp_err_t car_command_cb(const struct control_serial_command *));
esp_err_t httpd_stream_from_car_cb(struct control_status_report *status,
                                   void *user_context);
