#include "httpd.h"

#include <errno.h>
#include <stdatomic.h>

#include "esp_check.h"
#include "esp_http_server.h"
#include "esp_random.h"

#include "control_stream.h"
#include "html_static.h"
#include "httpd_camera.h"

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include "cJSON/cJSON.h"

#include "sdkconfig.h"

#define ERRBUF_MAXLEN (128)

// HTTPD handlers.
static httpd_handle_t httpd = NULL;
static esp_err_t (*car_cmd_send_cb)(const struct control_serial_command *) =
    NULL;

static esp_err_t httpd_handler_health(httpd_req_t *req) {
  const char resp[] = "ok";

  return httpd_resp_send(req, resp, sizeof(resp));
}

static httpd_uri_t httpd_uri_health = {
    .uri = HTTPD_PATH_HEALTH,
    .method = HTTP_GET,
    .handler = httpd_handler_health,
};

// Control web socket
#define CONTROL_LOG_TAG "HTTPD_CONTROL"
#define CONTROL_MSG_MAXLEN (128)
#define CONTROL_WORKER_STACK (2048)
#define CONTROL_WORKER_MAX_CLIENTS (CONFIG_LWIP_MAX_SOCKETS)
#define CONTROL_STREAM_FROM_CAR_INT (5000)
#define CONTROL_STREAM_FROM_CAR_WAIT_STATUS (5000)

esp_err_t httpd_stream_from_car_cb(struct control_status_report *status,
                                   void *unuzed) {
  if (httpd == NULL) {
    ESP_RETURN_ON_ERROR(ESP_FAIL, CONTROL_LOG_TAG,
                        "httpd_stream_from_car_cb: HTTPD is not initialized");
  }

  esp_err_t ret = ESP_OK;
  char err_buf[ERRBUF_MAXLEN] = {0};

  size_t fds = CONTROL_WORKER_MAX_CLIENTS;
  int client_fds[CONTROL_WORKER_MAX_CLIENTS];
  ret = httpd_get_client_list(httpd, &fds, client_fds);
  ESP_RETURN_ON_ERROR(ret, CONTROL_LOG_TAG, "Can not get client FDs: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  // Data.
  char payload[CONTROL_MSG_MAXLEN];
  int status_len =
      snprintf(payload, CONTROL_MSG_MAXLEN,
               "{\"battery\":%f,\"speed\":%i,\"servo\":%i,\"sonar_distance\":%"
               "i,\"uptime_ms\":%li}",
               status->battery_voltage, status->car_speed, status->servo_angle,
               status->sonar_distance, status->uptime);
  if (status_len > CONTROL_MSG_MAXLEN) {
    ret = ESP_ERR_NO_MEM;
    ESP_RETURN_ON_ERROR(ret, CONTROL_LOG_TAG,
                        "Car status message is longer than the buffer: %s",
                        esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
  }
  if (status_len < 0) {
    ret = ESP_FAIL;
    ESP_RETURN_ON_ERROR(ret, CONTROL_LOG_TAG,
                        "Can not format car status message, error %i",
                        status_len);
  }

  for (size_t i = 0; i < fds; i++) {
    int fd = client_fds[i];

    if (httpd_ws_get_fd_info(httpd, fd) == HTTPD_WS_CLIENT_WEBSOCKET) {
      ESP_LOGD(CONTROL_LOG_TAG, "Sending to WS client %d", fd);

      httpd_ws_frame_t pkt = {
          .type = HTTPD_WS_TYPE_TEXT,
          .payload = (uint8_t *)payload,
          .len = status_len,
      };
      ret = httpd_ws_send_data(httpd, fd, &pkt);
      if (ret != ESP_OK) {
        ESP_LOGE(CONTROL_LOG_TAG, "Can not send to client: %s",
                 esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
      }
    }
  }

  return ret;
}

static esp_err_t control_stream_handler(httpd_req_t *req) {
  esp_err_t ret = ESP_OK;

  if (req->method == HTTP_GET) {
    ESP_LOGI("WS", "Handshake done, the new connection was opened");
    return ESP_OK;
  }

  // httpd_ws_frame_t pkt = {0};
  httpd_ws_frame_t pkt = {0};
  pkt.type = HTTPD_WS_TYPE_TEXT;

  ret = httpd_ws_recv_frame(req, &pkt, 0);
  ESP_GOTO_ON_ERROR(ret, free_mem, CONTROL_STREAM_LOG_TAG,
                    "Can not get WebSocket frame length from a client: %d",
                    ret);

  ESP_LOGI(CONTROL_STREAM_LOG_TAG, "WS meta: len=%zu", pkt.len);
  if (pkt.len > CONTROL_MSG_MAXLEN) {
    ESP_GOTO_ON_ERROR(
        ESP_ERR_NO_MEM, free_mem, CONTROL_STREAM_LOG_TAG,
        "Web socket packet length is larger than maximum allowed %i",
        CONTROL_MSG_MAXLEN);
  }

  pkt.payload = calloc(pkt.len + 1, sizeof(*pkt.payload));
  if (pkt.payload == NULL) {
    ESP_GOTO_ON_ERROR(errno, free_mem, "WS",
                      "Can not allocate buffer for WS message");
  }

  ret = httpd_ws_recv_frame(req, &pkt, pkt.len);
  char err_buf[ERRBUF_MAXLEN] = {0};
  ESP_GOTO_ON_ERROR(ret, free_mem, CONTROL_STREAM_LOG_TAG,
                    "Can not get WebSocket frame from a client: %d: %s", ret,
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
  ESP_LOGI(CONTROL_STREAM_LOG_TAG, "Printing websocket command");
  ESP_LOGI(CONTROL_STREAM_LOG_TAG, "WS data: \"%s\", final=%d",
           (char *)pkt.payload, pkt.final);

  struct control_serial_command cmd = {0};

  ret = control_load_json((const char *)pkt.payload, pkt.len, &cmd);
  ESP_GOTO_ON_ERROR(ret, free_mem, CONTROL_STREAM_LOG_TAG,
                    "Can not load command from a client: %s",
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

  if (car_cmd_send_cb == NULL) {
    ret = ESP_FAIL;
    ESP_GOTO_ON_ERROR(ret, free_mem, CONTROL_STREAM_LOG_TAG,
                      "Can not send command to the car: command send function "
                      "pointer is NULL");
  }

  ret = car_cmd_send_cb(&cmd);
  ESP_GOTO_ON_ERROR(ret, free_mem, CONTROL_STREAM_LOG_TAG,
                    "Can not send command to the car: %s",
                    esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));

free_mem:
  free(pkt.payload);

  return ret;
}

static httpd_uri_t httpd_uri_control_stream = {
    .uri = HTTPD_PATH_CONTROL_STREAM,
    .method = HTTP_GET,
    .handler = control_stream_handler,
    .is_websocket = true,
};

static esp_err_t configure_wifi_handler(httpd_req_t *req) {
  esp_err_t ret = ESP_OK;
  char err_buf[ERRBUF_MAXLEN] = {0};
// WiFi configuration message should not be longer than SSID + PASSWORD + MODE
// + JSON overhead =>
// { "ssid": "<32 bytes>", "password": <64 bytes>, "mode": <1 byte> } =>
// ~ 137 bytes so 256 bytes is more than enough.
#define __REQ_MAXLEN (256)
  char buf[__REQ_MAXLEN] = {0};

  int read_bytes = httpd_req_recv(req, buf, __REQ_MAXLEN);
  if (read_bytes <= 0) {
    ret = ESP_FAIL;
    ESP_RETURN_ON_ERROR(ret, HTTPD_LOG_TAG " configure_wifi_handler:",
                        "Can not read request: read bytes: %d", read_bytes);
  }

  // Read and parse JSON.
  cJSON *wifi_json = cJSON_ParseWithLength(buf, read_bytes);
  if (wifi_json == NULL) {
    ret = 400;
    httpd_resp_send_err(req, ret, "Can not parse JSON payload");
    ESP_LOGW(HTTPD_LOG_TAG " configure_wifi_handler:",
             "Can not parse JSON, error at character %i",
             cJSON_GetErrorPtr() - buf);
    return ret;
  }

  // SSID.
  cJSON *ssid_json = cJSON_GetObjectItem(wifi_json, "ssid");
  if (ssid_json == NULL) {
    ret = 400;
    httpd_resp_send_err(req, ret, "JSON payload does not have \"ssid\" field");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "JSON payload does not have \"ssid\" field");
  }

  char *ssid = cJSON_GetStringValue(ssid_json);
  ret = wifi_validate_ssid(ssid);
  if (ret != ESP_OK) {
    ret = 400;
    httpd_resp_send_err(req, ret,
                        "SSID value is not correct (must be 8-32 characters)");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "Password value length is not in range [%d, %d]",
                      WIFI_SSID_MINLEN, WIFI_SSID_MAXLEN);
  }

  // Password.
  cJSON *password_json = cJSON_GetObjectItem(wifi_json, "password");
  if (password_json == NULL) {
    ret = 400;
    httpd_resp_send_err(req, ret,
                        "JSON payload does not have \"password\" field");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "JSON payload does not have \"password\" field");
  }

  char *password = cJSON_GetStringValue(password_json);
  ret = wifi_validate_password(password);
  if (ret != ESP_OK) {
    ret = 400;
    httpd_resp_send_err(
        req, ret, "Password value is not correct (must be 8-64 characters)");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "Password value length is not in range [%d, %d]",
                      WIFI_PASSWORD_MINLEN, WIFI_PASSWORD_MAXLEN);
  }

  // Mode.
  cJSON *mode_json = cJSON_GetObjectItem(wifi_json, "mode");
  if (mode_json == NULL) {
    ret = 400;
    httpd_resp_send_err(req, ret, "JSON payload does not have \"mode\" field");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "JSON payload does not have \"mode\" field");
  }

  int mode = cJSON_GetNumberValue(mode_json);
  ret = wifi_validate_mode(mode);
  if (ret != ESP_OK) {
    ret = 400;
    httpd_resp_send_err(
        req, ret, "WiFi mode value is not correct (must be either 1 or 2)");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG " configure_wifi_handler:",
                      "WiFi mode value is not correct, value %d", mode);
  }

  ret = wifi_set_persistent_config(ssid, password, mode);
  if (ret != ESP_OK) {
    httpd_resp_send_err(req, 500, "Can not save WiFi settings");
    ESP_GOTO_ON_ERROR(ret, free_mem, HTTPD_LOG_TAG "configure_wifi_handler:",
                      "Can not save WiFi settings: %s",
                      esp_err_to_name_r(ret, err_buf, ERRBUF_MAXLEN));
  }

free_mem:
  cJSON_Delete(wifi_json);

  const char succ_resp[] = "{\"status\": 200}";
  return httpd_resp_send(req, succ_resp, strlen(succ_resp));
}

static httpd_uri_t httpd_uri_configure_wifi = {
    .uri = HTTPD_PATH_CONFIGURE_WIFI,
    .method = HTTP_POST,
    .handler = configure_wifi_handler,
};

static esp_err_t reboot_handler(httpd_req_t *req) {
  const char reboot_resp[] = "restarting";
  httpd_resp_send(req, reboot_resp, strlen(reboot_resp));
  esp_restart();
}

static httpd_uri_t httpd_uri_reboot = {
    .uri = HTTPD_PATH_REBOOT,
    .method = HTTP_POST,
    .handler = reboot_handler,
};

void httpd_stop_server() {
  // This function returns error only on NULL arg, so it is ignored.
  httpd_stop(httpd);
  httpd_camera_stop_workers();

  httpd = NULL;
}

esp_err_t
httpd_init(unsigned int httpd_prio, unsigned int camera_prio, int max_sockets,
           esp_err_t car_command_cb(const struct control_serial_command *)) {
  // HTTPD.
  esp_err_t ret = 0;

  ESP_LOGI(HTTPD_LOG_TAG, "Starting HTTPD server");

  httpd = NULL;
  httpd_config_t httpd_config = HTTPD_DEFAULT_CONFIG();

  httpd_config.task_priority = httpd_prio;
  httpd_config.max_uri_handlers += HTML_STATIC_TOTAL_HANDLERS;
  // Max for LWIP is 16, 3 sockets are used by HTTPD itself
  httpd_config.max_open_sockets = max_sockets;
  httpd_config.backlog_conn = 50;

  ret = httpd_start(&httpd, &httpd_config);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG, "Can not start HTTPD");

  ESP_LOGI(HTTPD_LOG_TAG, "Registering health handler");
  ret = httpd_register_uri_handler(httpd, &httpd_uri_health);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register handler: " HTTPD_PATH_HEALTH);

  ESP_LOGI(HTTPD_LOG_TAG, "Registering WiFi configuration handler");
  ret = httpd_register_uri_handler(httpd, &httpd_uri_configure_wifi);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register handler: " HTTPD_PATH_CONFIGURE_WIFI);

  ESP_LOGI(HTTPD_LOG_TAG, "Registering reboot handler");
  ret = httpd_register_uri_handler(httpd, &httpd_uri_reboot);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register handler: " HTTPD_PATH_REBOOT);

  const int image_stream_workers = 1;
  ESP_LOGI(HTTPD_LOG_TAG, "Starting %d camera async workers",
           image_stream_workers);
  ret = httpd_camera_start_workers(image_stream_workers, camera_prio);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not start camera async workers");

  ESP_LOGI(HTTPD_LOG_TAG, "Registering camera image handlers");
  ret = httpd_camera_register_handlers(httpd, HTTPD_PATH_GET_IMG,
                                       HTTPD_PATH_STREAM_IMG);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register camera handlers");

  ESP_LOGI(HTTPD_LOG_TAG, "Registering control stream handler");
  car_cmd_send_cb = car_command_cb;
  ret = httpd_register_uri_handler(httpd, &httpd_uri_control_stream);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register handler: " HTTPD_PATH_CONTROL_STREAM);

  // Register static file handlers
  ESP_LOGI(HTTPD_LOG_TAG, "Registering static files handlers");
  ret = html_static_register_uri_handlers(httpd);
  ESP_GOTO_ON_ERROR(ret, cleanup, HTTPD_LOG_TAG,
                    "Can not register static file handlers");

  return ESP_OK;

cleanup:
  httpd_stop_server();
  return ret;
}